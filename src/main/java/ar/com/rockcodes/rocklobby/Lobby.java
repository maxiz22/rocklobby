package ar.com.rockcodes.rocklobby;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;

import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.rocklobby.commands.Comandos;
import ar.com.rockcodes.rocklobby.listeners.LobbyListener;
import ar.com.rockcodes.rocklobby.util.Menu;

public class Lobby extends JavaPlugin{

	public static Lobby plugin;
	@Override
	public void onDisable() {
	}

	@Override
	public void onEnable() {
		Lobby.plugin = this;
		saveDefaultConfig();
		this.reloadConfig();
		
		if(this.getServer().getPluginManager().isPluginEnabled("MultiInv")){
			this.getServer().getPluginManager().registerEvents(new LobbyListener(),this);
			this.getCommand("hub").setExecutor(new Comandos());
		}else{
			this.getLogger().info("Error: MuliInv not detected disabling plugin...");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		
	}

	
	public Menu get_menu(){
		
		Menu menu  = new Menu(this, getConfig().getString("inventoryname"), getConfig().getInt("inventoryrows"));
		
		if(getConfig() == null) return menu;
		
		ConfigurationSection booksconf = getConfig().getConfigurationSection("items");
		
		for(String key : booksconf.getKeys(false)){
			
			ConfigurationSection bookconf = booksconf.getConfigurationSection(key);
			
			Material mat = Material.getMaterial(bookconf.getInt("icon"));
			
			int slot =bookconf.getInt("slot");
			ItemStack item = Menu.createItem(mat, bookconf.getInt("amount"), Lobby.colorizeText(bookconf.getString("name")),  Lobby.colorizeText(bookconf.getString("lore")),(short) 0);
			
			
			menu.setItem(slot,item);
			
			if(bookconf.getString("executeCommand") !="" && bookconf.getString("executeCommand") !=null){
				
				final String command = bookconf.getString("executeCommand") ;
				menu.setAction(slot, new Menu.ItemAction(){

					@Override
					public void run(Player player, Inventory inv, ItemStack item, int slot, InventoryAction action) {
						ejecutarComando(player,command);
					}

				});
				
			}

			
		}
		

		return menu;
		
	}
	
	public static String colorizeText(String string) {
	    string = string.replaceAll("&0", ChatColor.BLACK+"");
	    string = string.replaceAll("&1", ChatColor.DARK_BLUE+"");
	    string = string.replaceAll("&2", ChatColor.DARK_GREEN+"");
	    string = string.replaceAll("&3", ChatColor.DARK_AQUA+"");
	    string = string.replaceAll("&4", ChatColor.DARK_RED+"");
	    string = string.replaceAll("&5", ChatColor.DARK_PURPLE+"");
	    string = string.replaceAll("&6", ChatColor.GOLD+"");
	    string = string.replaceAll("&7", ChatColor.GRAY+"");
	    string = string.replaceAll("&8", ChatColor.DARK_GRAY+"");
	    string = string.replaceAll("&9", ChatColor.BLUE+"");
	    string = string.replaceAll("&a", ChatColor.GREEN+"");
	    string = string.replaceAll("&b", ChatColor.AQUA+"");
	    string = string.replaceAll("&c", ChatColor.RED+"");
	    string = string.replaceAll("&d", ChatColor.LIGHT_PURPLE+"");
	    string = string.replaceAll("&e", ChatColor.YELLOW+"");
	    string = string.replaceAll("&f", ChatColor.WHITE+"");
	    string = string.replaceAll("&l", ChatColor.BOLD+"");
	    return string;
	}
	
	private void ejecutarComando(Player player, String command) {
		//Parse player
		command = command.replace("&p", player.getName());
		//Execute command
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);		
	}
	
	private void ejecutarComandoPlayer(Player player, String command) {
		Bukkit.getServer().dispatchCommand(player, command);		
	}
}
	