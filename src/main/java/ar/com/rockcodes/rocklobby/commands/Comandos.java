package ar.com.rockcodes.rocklobby.commands;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ar.com.rockcodes.rocklobby.Lobby;

public class Comandos implements CommandExecutor{

	
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {

		if(args.length==0){command_main(sender,args); return true;}
			
		switch(args[0]){
			case "reload" : command_reload(sender,args); return true;
			default : command_main(sender,args); return true;
		}
			
		
	}

	private void command_reload(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if(!player.isOp()) return;
		}
		Lobby.plugin.reloadConfig();
		sender.sendMessage("Config reloaded");
		Lobby.plugin.getLogger().info("Config reloaded");
	}

	private void command_main(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player)sender;
			World w = Bukkit.getWorld(Lobby.plugin.getConfig().getString("world"));
			player.teleport(w.getSpawnLocation());
		}
		
	}

}
