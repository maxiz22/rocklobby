package ar.com.rockcodes.rocklobby.listeners;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import ar.com.rockcodes.rocklobby.Lobby;
import ar.com.rockcodes.rocklobby.util.Menu;

import uk.co.tggl.pluckerpluck.multiinv.api.ChangeInventoryEvent;

public class LobbyListener implements Listener {

	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoinEvent(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if(!player.getWorld().getName().equalsIgnoreCase(Lobby.plugin.getConfig().getString("world")))return;
		this.setInventory(player);
	}
	
	private void setInventory(Player player){
		player.getInventory().clear();
		
		Material mat =  Material.getMaterial(Lobby.plugin.getConfig().getInt("hubitem"));
		String name =  Lobby.colorizeText(Lobby.plugin.getConfig().getString("hubitem-name"));
		String lore =  Lobby.colorizeText(Lobby.plugin.getConfig().getString("hubitem-lore"));
		ItemStack item = Menu.createItem(mat, 1, name, lore, (short)0);

		player.getInventory().addItem(item);
		
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onchangeinventory(ChangeInventoryEvent e){
	
		Player player = e.getPlayer();
		
		World worldto = e.getWorldTo();
		

		if(worldto.getName().equalsIgnoreCase(Lobby.plugin.getConfig().getString("world"))){
			this.setInventory(player);
		}
	}
	
	
	@EventHandler
    public void onItemDrop (PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if(Lobby.plugin.getConfig().getBoolean("allowdropitems",false)) return;
        if(!p.getWorld().getName().equalsIgnoreCase(Lobby.plugin.getConfig().getString("world")))return;
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(!p.getWorld().getName().equalsIgnoreCase(Lobby.plugin.getConfig().getString("world")))return;

        ItemStack item = e.getItem();
        if(item==null)return;
        Material mat =  Material.getMaterial(Lobby.plugin.getConfig().getInt("hubitem"));
        if(item.getType().equals(mat)){
        	
        	Lobby.plugin.get_menu().showMenu(p);
        	e.setCancelled(true);
        }
    }
    /*@EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        boolean n = event.getEntity().getItemStack().getType() == Material.BOWL;
            if(n == true){
                event.getEntity().remove();
               
            }  
    }*/
    
    
	
	
}
